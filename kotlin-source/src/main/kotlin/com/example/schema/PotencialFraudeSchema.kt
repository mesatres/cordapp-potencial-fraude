package com.example.schema

import net.corda.core.identity.AbstractParty
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.time.Instant
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table


object PotencialFraudeSchema

object PotencialFraudeSchemaV1 : MappedSchema(
        schemaFamily = PotencialFraudeSchema.javaClass,
        version = 1,
        mappedTypes = listOf(PotencialFraude::class.java)) {

    @Entity
    @Table(name = "potencial_fraude")
    class PotencialFraude(

            @Column(name = "linear_id") var linearId: UUID = UUID.randomUUID(),
            @Column(name = "origem") var origem: String = "",
            @Column(name = "destinos") var destinos: String = "",

            @Column(name = "cpf") var cpf: String = "",
            @Column(name = "cep") var cep: String = "",
            @Column(name = "rua") var rua: String = "",
            @Column(name = "numero") var numero: String = "",
            @Column(name = "complemento") var complemento: String = "",
            @Column(name = "bairro") var bairro: String = "",
            @Column(name = "cidade") var cidade: String = "",
            @Column(name = "uf") var uf: String = "",
            @Column(name = "celular") var celular: String = "",
            @Column(name = "telResidencial") var telResidencial: String = "",
            @Column(name = "dataNascimento") var dataNascimento: String = "",
            @Column(name = "email") var email: String = "",
            @Column(name = "nomeMae") var nomeMae: String = "",
            @Column(name = "profissao") var profissao: String = "",
            @Column(name = "rendaInferida") var rendaInferida: String = "",

            @Column(name = "device") var device: String = "",
            @Column(name = "ip") var ip: String = "",
            @Column(name = "userAgent") var userAgent: String = "",
            @Column(name = "so") var so: String = "",

            @Column(name = "dataHora") var dataHora: Instant = Instant.now(),
            @Column(name = "motivo") var motivo: String = ""

    ) : PersistentState()
}