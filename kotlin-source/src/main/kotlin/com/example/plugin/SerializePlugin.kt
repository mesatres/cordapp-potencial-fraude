package com.example.plugin

import com.example.dto.PotencialFraudeDTO
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.serialization.SerializationWhitelist
import net.corda.webserver.services.WebServerPluginRegistry

class SerializePlugin : SerializationWhitelist {
    override val whitelist: List<Class<*>>
        get() = listOf(PotencialFraudeDTO::class.java)
}