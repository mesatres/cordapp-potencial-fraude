package com.example.dto

class GetResponseDTO(
        var cpf: String = "",
        var email: String = "",
        var telefones: String = "",
        var endereco: String = "",
        var data: String = "",
        var motivo: String = "",
        var dispositivo: String = "",
        var origem: String = ""
)