package com.example.dto

import java.time.Instant

class PotencialFraudeDTO (

        val cpf: String,
        val cep: String,
        val rua: String,
        val numero: String,
        val complemento: String,
        val bairro: String,
        val cidade: String,
        val uf: String,
        val celular: String,
        val telResidencial: String,
        val dataNascimento: String,
        val email: String,
        val nomeMae: String,
        val profissao: String,
        val rendaInferida: String,

        val device: String,
        val ip: String,
        val userAgent: String,
        val so: String,

        val dataHora: Instant = Instant.now(),
        val motivo: String

)