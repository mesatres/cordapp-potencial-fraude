package com.example.contract

import com.example.state.IOUState
import com.example.state.PotencialFraudeState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.CommandWithParties
import net.corda.core.contracts.Contract
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction
import java.time.Instant
import java.time.temporal.ChronoUnit

open class PotencialFraudeContract : Contract {

    companion object {
        @JvmStatic
        val IOU_CONTRACT_ID = "com.example.contract.PotencialFraudeContract"
    }

    override fun verify(tx: LedgerTransaction) {

        val command = tx.commands.single()

        requireThat {

            val outputs = tx.outputsOfType<PotencialFraudeState>().single()
            val potencialFraude = outputs.potencialFraude;

            // GARANTIR QUE OS CAMPOS OBRIGATORIOS ESTAO PREENCHIDOS
            "Nenhum campo foi preenchido." using (!tx.outputs.isEmpty())

            "O campo 'device' nao foi preenchido." using (!potencialFraude.device.isEmpty())
            "O campo 'ip' nao foi preenchido." using (!potencialFraude.ip.isEmpty())
            "O campo 'userAgent' nao foi preenchido." using (!potencialFraude.userAgent.isEmpty())
            "O campo 'so' nao foi preenchido." using (!potencialFraude.so.isEmpty())
            "O campo 'motivo' nao foi preenchido." using (!potencialFraude.motivo.isEmpty())

//            "Somente quem assina o comando pode alterar um estado" using
//                    (command.signers.contains(outputs.origem.owningKey))
        }

    }

    interface Commands : CommandData {
        class INSERT : Commands
        class UPDATE : Commands
    }

}