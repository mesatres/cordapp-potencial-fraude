package com.example.api

import com.example.dto.GetResponseDTO
import com.example.dto.PotencialFraudeDTO
import com.example.flow.ExampleFlow.Initiator
import com.example.flow.PotencialFraudeFlow
import com.example.schema.PotencialFraudeSchemaV1
import com.example.state.IOUState
import com.example.state.PotencialFraudeState
import net.corda.core.identity.CordaX500Name
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.messaging.startTrackedFlow
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.loggerFor
import org.slf4j.Logger
import java.text.SimpleDateFormat
import java.util.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.core.Response.Status.CREATED

val SERVICE_NAMES = listOf("Controller", "Network Map Service")

@Path("example")
class ExampleApi(private val rpcOps: CordaRPCOps) {

    private val myLegalName: CordaX500Name = rpcOps.nodeInfo().legalIdentities.first().name

    companion object {
        private val logger: Logger = loggerFor<ExampleApi>()
    }

    @GET
    @Path("me")
    @Produces(MediaType.APPLICATION_JSON)
    fun whoami() = mapOf("me" to myLegalName)

    @GET
    @Path("peers")
    @Produces(MediaType.APPLICATION_JSON)
    fun getPeers(): Map<String, List<CordaX500Name>> {
        val nodeInfo = rpcOps.networkMapSnapshot()
        return mapOf("peers" to nodeInfo
                .map { it.legalIdentities.first().name }
                .filter { it.organisation !in (SERVICE_NAMES + myLegalName.organisation) })
    }

    @GET
    @Path("iou/all")
    @Produces(MediaType.APPLICATION_JSON)
    fun all() = rpcOps.vaultQueryBy<PotencialFraudeState>().states

    @POST
    @Path("iou")
    @Consumes(MediaType.APPLICATION_JSON)
    fun post(potencialFraude: PotencialFraudeDTO): Response {
        return try {
            val flowHandle = rpcOps.startTrackedFlow(PotencialFraudeFlow::Initiator
                    , potencialFraude
            )

            flowHandle.progress.subscribe { println(">> $it") }

            // The line below blocks and waits for the future to resolve.
            val result = flowHandle.returnValue.getOrThrow()

            Response.status(Response.Status.CREATED).entity("Transaction id ${result.id} committed to ledger.\n").build()

        } catch (ex: Throwable) {
            logger.error(ex.message, ex)
            Response.status(Response.Status.BAD_REQUEST).entity(ex.message!!).build()
        }
    }

    @GET
    @Path("iou")
    @Produces(MediaType.APPLICATION_JSON)
    fun get(@QueryParam("cpf") @DefaultValue("") cpf: String,
            @QueryParam("email") @DefaultValue("") email: String,
            @QueryParam("motivo") @DefaultValue("") motivo: String) : List<GetResponseDTO> {
        return rpcOps.vaultQueryBy<PotencialFraudeState>().states
                .map { it.state.data }
                .filter { applyFilter(it, cpf, email, motivo) }
                .map { getResponseDTO(it) }
    }

    fun applyFilter(state: PotencialFraudeState, cpf: String, email: String, motivo: String) : Boolean {
        return  (cpf.trim() == "" && email.trim() == "" && motivo.trim() == "") ||
                (cpf.trim() != "" && state.potencialFraude.cpf.trim().contains(cpf)) ||
                (email.trim() != "" && state.potencialFraude.email.trim().toLowerCase().contains(email.trim().toLowerCase())) ||
                (motivo.trim() != "" && state.potencialFraude.motivo.trim().toLowerCase().contains(motivo.trim().toLowerCase()))
    }

    fun getResponseDTO(state: PotencialFraudeState) : GetResponseDTO {

        var response = GetResponseDTO()

        response.cpf = state.potencialFraude.cpf
        response.email = state.potencialFraude.email
        response.telefones = "Res.: " + state.potencialFraude.telResidencial + ", Cel.: " + state.potencialFraude.celular

        response.endereco =
                state.potencialFraude.rua + ", " +
                        state.potencialFraude.numero + ", " +
                        state.potencialFraude.bairro + ", " +
                        state.potencialFraude.cidade + ", " +
                        state.potencialFraude.uf + ", " +
                        state.potencialFraude.cep

        response.data =
                SimpleDateFormat("dd/MM/yyyy HH:mm")
                        .format(Date(state.potencialFraude.dataHora.toEpochMilli()))

        response.motivo = state.potencialFraude.motivo

        response.dispositivo =
                "Device: " + state.potencialFraude.device +
                        ", IP: " + state.potencialFraude.ip +
                        ", UserAgent: " + state.potencialFraude.userAgent +
                        ", SO: " + state.potencialFraude.so

        response.origem = state.origem.name.toString()

        return response

    }

}