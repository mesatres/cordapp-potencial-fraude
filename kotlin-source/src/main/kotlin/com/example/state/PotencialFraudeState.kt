package com.example.state

import com.example.dto.PotencialFraudeDTO
import com.example.schema.IOUSchemaV1
import com.example.schema.PotencialFraudeSchemaV1
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import java.time.Instant

data class PotencialFraudeState(var potencialFraude: PotencialFraudeDTO,
                                var origem : Party,
                                var destinos : List<Party>,
                                override val linearId: UniqueIdentifier = UniqueIdentifier())
    : LinearState, QueryableState {

    override val participants: List<AbstractParty> get() = destinos.plusElement(origem)

    override fun generateMappedObject(schema: MappedSchema): PersistentState {

        return when (schema) {
            is PotencialFraudeSchemaV1 -> {
                
                var entity = PotencialFraudeSchemaV1.PotencialFraude()
                
                entity.linearId = this.linearId.id
                entity.origem = this.origem.name.toString()
                entity.destinos = this.destinos.map{ it.name.toString() }.reduce{ resultado, name -> name + ":" + resultado }

                entity.cpf = this.potencialFraude.cpf
                entity.cep = this.potencialFraude.cep
                entity.rua = this.potencialFraude.rua
                entity.numero = this.potencialFraude.numero
                entity.complemento = this.potencialFraude.complemento
                entity.bairro = this.potencialFraude.bairro
                entity.cidade = this.potencialFraude.cidade
                entity.uf = this.potencialFraude.uf
                entity.celular = this.potencialFraude.celular
                entity.telResidencial = this.potencialFraude.telResidencial
                entity.dataNascimento = this.potencialFraude.dataNascimento
                entity.email = this.potencialFraude.email
                entity.nomeMae = this.potencialFraude.nomeMae
                entity.profissao = this.potencialFraude.profissao
                entity.rendaInferida = this.potencialFraude.rendaInferida

                entity.device = this.potencialFraude.device
                entity.ip = this.potencialFraude.ip
                entity.userAgent = this.potencialFraude.userAgent
                entity.so = this.potencialFraude.so

                entity.dataHora = this.potencialFraude.dataHora
                entity.motivo = this.potencialFraude.motivo

                return entity
            }
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(PotencialFraudeSchemaV1)
}