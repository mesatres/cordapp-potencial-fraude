package com.example.flow

import co.paralleluniverse.fibers.Suspendable
import com.example.contract.PotencialFraudeContract
import com.example.dto.PotencialFraudeDTO
import com.example.state.PotencialFraudeState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker

object PotencialFraudeFlow {

    @InitiatingFlow
    @StartableByRPC
    class Initiator(val potencialFraude: PotencialFraudeDTO) : FlowLogic<SignedTransaction>() {

        companion object {

            object GENERATING_TRANSACTION : ProgressTracker.Step("Generating transaction based on new IOU.")
            object VERIFYING_TRANSACTION : ProgressTracker.Step("Verifying contract constraints.")
            object SIGNING_TRANSACTION : ProgressTracker.Step("Signing transaction with our private key.")
            object GATHERING_SIGS : ProgressTracker.Step("Gathering the counterparty's signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }


            object FINALISING_TRANSACTION : ProgressTracker.Step("Obtaining notary signature and recording transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(

                    GENERATING_TRANSACTION,
                    VERIFYING_TRANSACTION,
                    SIGNING_TRANSACTION,
                    GATHERING_SIGS,
                    FINALISING_TRANSACTION
            )
        }

        override val progressTracker = tracker()

        @Suspendable
        override fun call(): SignedTransaction {
            // Stage 1.
            progressTracker.currentStep = GENERATING_TRANSACTION

            val notary = serviceHub.networkMapCache.notaryIdentities[0]
            val origem = serviceHub.myInfo.legalIdentities.single()
            val destinos = serviceHub.networkMapCache.allNodes
                            .map { it.legalIdentities.first() }
                            .filter { it != origem && it != notary }

            val state = PotencialFraudeState(potencialFraude, origem, destinos)

            val txCommand = Command(PotencialFraudeContract.Commands.INSERT(),
                    destinos.map { it.owningKey })

            val txBuilder = TransactionBuilder(notary).withItems(
                    StateAndContract(state, PotencialFraudeContract.IOU_CONTRACT_ID),
                    txCommand)

            // Stage 2.
            progressTracker.currentStep = VERIFYING_TRANSACTION
            txBuilder.verify(serviceHub)

            // Stage 3.
            progressTracker.currentStep = SIGNING_TRANSACTION
            val partSignedTx = serviceHub.signInitialTransaction(txBuilder)

            // Stage 4.
            progressTracker.currentStep = GATHERING_SIGS
            val sessions = destinos.map { initiateFlow(it) }
            val fullySignedTx = subFlow(CollectSignaturesFlow(partSignedTx, sessions))

            // Stage 5.
            return subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
        }
    }

    @InitiatedBy(Initiator::class)
    class Acceptor(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {

        @Suspendable
        override fun call(): SignedTransaction {
            System.out.println("oi4")
            val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    val output = stx.tx.outputs.single().data
                    "A transacao precisa ter dados de uma fraude potencial." using
                            (output is PotencialFraudeState)
                }
            }
            System.out.println("oi4.5")
            return subFlow(signTransactionFlow)
        }
    }

}